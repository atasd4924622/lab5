﻿using System.Diagnostics;
class Program
{
    static void Sort(int[] array)
    {
        int n = array.Length;
        for (int i = 1; i < n; ++i)
        {
            int key = array[i];
            int j = i - 1;

            while (j >= 0 && array[j] > key)
            {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = key;
        }
    }

    static void Print(int[] array)
    {
        foreach (int item in array)
        {
            Console.Write(item + " ");
        }
        Console.WriteLine();
    }

    static void Main(string[] args)
    {
        Console.WriteLine("Введіть мінімальне значення діапазону:");
        int minValue = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Введіть максимальне значення діапазону:");
        int maxValue = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Введіть кількість випадкових значень:");
        int count = Convert.ToInt32(Console.ReadLine());

        Random rand = new Random();
        int[] array = new int[count];
        for (int i = 0; i < count; i++)
        {
            array[i] = rand.Next(minValue, maxValue + 1);
        }

        Console.WriteLine("Вихідний масив:");
        Print(array);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        Sort(array);
        stopwatch.Stop();
        long elapsedTicks = stopwatch.ElapsedTicks;
        long elapsedNanoSeconds = elapsedTicks * (1000000000L / Stopwatch.Frequency);
        Console.WriteLine("Відсортований масив:");
        Print(array);
        Console.WriteLine($"Час виконання сортування: {elapsedNanoSeconds} наносекунд");
    }
}