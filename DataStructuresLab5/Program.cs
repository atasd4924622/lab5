﻿using System.Diagnostics;
class Program
{
    static Random random = new Random();

    static LinkedList<int> CreateRandomList(int n, int min, int max)
    {
        LinkedList<int> list = new LinkedList<int>();
        for (int i = 0; i < n; i++)
        {
            list.AddLast(random.Next(min, max + 1));
        }
        return list;
    }

    // Функція для сортування списку методом вибору
    static void SelectionSort(LinkedList<int> list)
    {
        LinkedListNode<int> i, j, min;
        for (i = list.First; i != null; i = i.Next)
        {
            min = i;
            for (j = i.Next; j != null; j = j.Next)
            {
                if (min.Value > j.Value)
                {
                    min = j;
                }
            }
            if (min != i)
            {
                int temp = i.Value;
                i.Value = min.Value;
                min.Value = temp;
            }
        }
    }


    // Функція для виводу списку
    static void PrintList(LinkedList<int> list)
    {
        foreach (int item in list)
        {
            Console.Write(item + " ");
        }
        Console.WriteLine();
    }

    static void Main(string[] args)
    {
        Console.WriteLine("Введіть кількість випадкових чисел:");
        int n = int.Parse(Console.ReadLine());

        Console.WriteLine("Введіть мінімальне значення діапазону:");
        int min = int.Parse(Console.ReadLine());

        Console.WriteLine("Введіть максимальне значення діапазону:");
        int max = int.Parse(Console.ReadLine());

        LinkedList<int> list = CreateRandomList(n, min, max);

        Console.WriteLine("До сортування:");
        Stopwatch stopwatch = new Stopwatch();
        PrintList(list);
        stopwatch.Start();
        SelectionSort(list);
        stopwatch.Stop();
        long elapsedTicks = stopwatch.ElapsedTicks;
        long elapsedNanoSeconds = elapsedTicks * (1000000000L / Stopwatch.Frequency);
        Console.WriteLine("Після сортування:");
        PrintList(list);
        Console.WriteLine($"Час виконання сортування: {elapsedNanoSeconds} наносекунд");
    }
}