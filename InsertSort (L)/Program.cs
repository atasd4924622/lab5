﻿using System.Diagnostics;
class Program
{
   static Random rand = new Random();

    static LinkedList<int> CreateRandomList(int n, int min, int max)
    {
        LinkedList<int> list = new LinkedList<int>();
        for (int i = 0; i < n; i++)
        {
            list.AddLast(rand.Next(min, max + 1));
        }
        return list;
    }
    static void Sort(LinkedList<int> list)
    {
        var current = list.First;

        while (current != null)
        {
            var next = current.Next;
            var temp = current.Value;

            var marker = current.Previous;
            while (marker != null && marker.Value > temp)
            {
                marker = marker.Previous;
            }

            if (marker == null)
            {
                list.Remove(current);
                list.AddFirst(temp);
            }
            else
            {
                list.Remove(current);
                list.AddAfter(marker, temp);
            }

            current = next;
        }
    }

    static void PrintList(LinkedList<int> list)
    {
        foreach (int item in list)
        {
            Console.Write(item + " ");
        }
        Console.WriteLine();
    }

    static void Main(string[] args)
    {
        Console.WriteLine("Введіть мінімальне значення діапазону:");
        int minValue = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Введіть максимальне значення діапазону:");
        int maxValue = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Введіть кількість випадкових значень:");
        int count = Convert.ToInt32(Console.ReadLine());

        LinkedList<int> list = CreateRandomList(count, minValue, maxValue);

        Console.WriteLine("Список до сортування:");
        PrintList(list);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        Sort(list);
        stopwatch.Stop();
        long elapsedTicks = stopwatch.ElapsedTicks;
        long elapsedNanoSeconds = elapsedTicks * (1000000000L / Stopwatch.Frequency);
        Console.WriteLine("Відсортований масив:");
        PrintList(list);
        Console.WriteLine($"Час виконання сортування: {elapsedNanoSeconds} наносекунд");
    }
}